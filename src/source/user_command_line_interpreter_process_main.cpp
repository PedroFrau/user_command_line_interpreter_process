#include "user_command_line_interpreter_process.h"

int main(int argc, char **argv)
{
  ros::init(argc, argv, ros::this_node::getName());
  UserCommandLineInterpreter my_user_command_line_interpreter;
  my_user_command_line_interpreter.setUp();
  my_user_command_line_interpreter.start();
  ros::Rate loop_rate(10);
  while(ros::ok())
  {
    my_user_command_line_interpreter.run();
    ros::spinOnce();
    loop_rate.sleep();
  }
  return 0;
}