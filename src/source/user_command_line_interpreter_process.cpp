#include "user_command_line_interpreter_process.h"

UserCommandLineInterpreter::UserCommandLineInterpreter()
{
}
UserCommandLineInterpreter::~UserCommandLineInterpreter()
{
}

void UserCommandLineInterpreter::ownSetUp()
{
  ros::param::get("~drone_id", drone_id);
  if ( drone_id.length() == 0)
  {
    drone_id = "1";
  }
  ros::param::get("~order_topic", order_topic);
  if (order_topic.length() == 0)
  {
    order_topic = "operatorMessage";
  }
  ros::param::get("~current_pose_topic", current_pose_topic);
  if (current_pose_topic.length() == 0)
  {
    current_pose_topic = "EstimatedPose_droneGMR_wrt_GFF";
  }
  ros::param::get("~notification_topic", notification_topic);
  if (notification_topic.length() == 0)
  {
    notification_topic = "notification";
  }
  std::cout << COLOR_GREEN << "Enter an order (type \"help\" for a list of orders): " << COLOR_RESET << std::endl;
}

void UserCommandLineInterpreter::ownStart()
{
	//Thread
	order_thread = std::thread(&UserCommandLineInterpreter::getOrder, this);

	//Subscribers
	current_pose_subscriber = 
		n.subscribe(current_pose_topic, 1000, &UserCommandLineInterpreter::currentPoseCallback, this);
	notification_subscriber = 
		n.subscribe(notification_topic, 1000, &UserCommandLineInterpreter::notificationCallback, this);

	//Publishers
	order_pub = n.advertise<std_msgs::String>(order_topic, 1);
}

void UserCommandLineInterpreter::ownStop()
{
	current_pose_subscriber.shutdown();
	notification_subscriber.shutdown();
	order_pub.shutdown();
	order_thread.join();
}

void UserCommandLineInterpreter::ownRun()
{
	std::vector<std::string> orders_list = {"cm", "cms", "sp", "to", "l", "gh", "cp", "zi", "zo", "sm", "fm", "mu", "md", 
											"ml", "mr", "s", "ton", "toff"};


	if (order_msg != "")
	{
		if(order_msg == "help")
		{
			std::cout << "---------------------------------------------------" << std::endl;
			std::cout << COLOR_BLUE << "[INFO] - Orders list: " << COLOR_RESET << std::endl;
			std::cout << "- Take Off: to" << std::endl;
			std::cout << "- Land: l" << std::endl;
			std::cout << "- Go Home: gh" << std::endl;
			std::cout << "- Call Painter: cp" << std::endl;
			std::cout << "- Zoom In: zi" << std::endl;
			std::cout << "- Zoom Out: zo" << std::endl;
			std::cout << "- Continue Mission: cm" << std::endl;
			std::cout << "- Start Mission. sm" << std::endl;
			std::cout << "- Finish Mission: fm" << std::endl;
			std::cout << "- Move Up/Down/Left/Right: mu, md, ml, mr" << std::endl;
			std::cout << "- Stop: s" << std::endl;
			std::cout << "- Send Current Position: sp" << std::endl;
			std::cout << "- Turn Light On: ton" << std::endl;
			std::cout << "- Turn Light Off: toff" << std::endl;
			std::cout << "- Change Movement Strategy: cms" << std::endl;
			std::cout << "---------------------------------------------------" << std::endl;
		}
		
		else if(order_msg == "zz" || order_msg == "ud" || check)
		{	
			if(order_msg == "zz")
			{
				std::cout << COLOR_GREEN << "[INFO] - Using ZIG ZAG strategy." << COLOR_RESET << std::endl;
				std_msgs::String order_msg_t;
				order_msg_t.data = order_msg;
				order_pub.publish(order_msg_t);
				strategy = false;
				std::cout << COLOR_GREEN << "Enter an order (type \"help\" for a list of orders): " << COLOR_RESET << std::endl;
			}
			else if(order_msg == "ud")
			{
				std::cout << COLOR_GREEN << "[INFO] - Using UP and DOWN strategy." << COLOR_RESET << std::endl;
				std_msgs::String order_msg_t;
				order_msg_t.data = order_msg;
				order_pub.publish(order_msg_t);
				strategy = true;
				std::cout << COLOR_GREEN << "Enter an order (type \"help\" for a list of orders): " << COLOR_RESET << std::endl;
			}
			else
			{
				std::cout << COLOR_RED << "[ERROR] - Invalid strategy" << COLOR_RESET << std::endl;
				std::cout << COLOR_GREEN << "Enter an order (type \"help\" for a list of orders): " << COLOR_RESET << std::endl;
			}
			check = false;
		}
		else if(order_msg == "sp")
		{
			std::cout << COLOR_BLUE << "[INFO] - Your current position is [" << current_pose.x << ", "
									<< current_pose.y << ", " << current_pose.z << "]" << COLOR_RESET << std::endl;
			std::cout << COLOR_GREEN << "Enter an order (type \"help\" for a list of orders): " << COLOR_RESET << std::endl;

		}
		else if(std::find(std::begin(orders_list), std::end(orders_list), order_msg) == std::end(orders_list))
		{
			std::cout << COLOR_RED << "[ERROR] - Your order could not be recognized. Please type \"help\" "
								   << "for a list of orders" << COLOR_RESET <<std::endl;
			std::cout << COLOR_GREEN << "Enter an order (type \"help\" for a list of orders): " << COLOR_RESET << std::endl;

		}
		else if(order_msg == "cm")
		{
			std_msgs::String continue_mission;
			continue_mission.data = "cm";
			order_pub.publish(continue_mission);
		}
		else if(std::find(std::begin(orders_list), std::end(orders_list), order_msg) != std::end(orders_list) 
			&& order_msg != "cm" && order_msg != "zz" && order_msg != "ud" && order_msg != "sp")
		{
			std_msgs::String order_msg_t;
			order_msg_t.data = order_msg;
			order_pub.publish(order_msg_t);
		}
		order_msg = "";
	}
}

void UserCommandLineInterpreter::currentPoseCallback(const droneMsgsROS::dronePose &msg)
{
	current_pose = msg;
}

void UserCommandLineInterpreter::notificationCallback(const std_msgs::String &msg)
{
	std::cout << COLOR_GREEN << "\n [NOTIFICATION] - " << msg << COLOR_RESET << std::endl;
	std::cout << COLOR_GREEN << "Enter an order (type \"help\" for a list of orders): " << COLOR_RESET << std::endl;
}

void UserCommandLineInterpreter::getOrder()
{
	//std::cout << COLOR_GREEN << "Enter an order (type \"help\" for a list of orders): " << COLOR_RESET << std::endl;
	while(std::getline(std::cin, order))
	{	
		std::cout << COLOR_GREEN << "Enter an order (type \"help\" for a list of orders): " << COLOR_RESET << std::endl;
		order_msg = order;

		if(order_msg == "cms")
		{
			std::cout << COLOR_BLUE <<
			  "[QUESTION] - Which strategy would you like to use? (ZIG_ZAG: zz/UP_DOWN: ud)"
			  << COLOR_RESET<< std::endl;
			if (std::getline(std::cin, order))
			{
				order_msg = order;
				check = true;			
			}
		}
		order = "";
	}
}


