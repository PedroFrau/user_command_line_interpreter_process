/*!*****************************************************************************
 *  \file    user_command_line_interpreter_process.h
 *  \brief   Definition of all the classes used in the file
 *           user_command_line_interpreter_process.cpp .
 *
 *  \author  Pedro Frau
 *  \copyright Copyright 2017 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/

#ifndef USER_COMMAND_LINE_INTERPRETER_PROCESS
#define USER_COMMAND_LINE_INTERPRETER_PROCESS

// ROS
#include "ros/ros.h"

// C++ standar libraries
#include <cstdlib>
#include <stdio.h>
#include <iostream>
#include <time.h>
#include <fstream>
#include <vector>
#include <list>
#include <set>
#include <algorithm>
#include <sstream>
#include <string>
#include <thread>

// droneMsgsROS
#include "droneMsgsROS/droneNavData.h"
#include "droneMsgsROS/societyPose.h"
#include "droneMsgsROS/droneInfo.h"
#include "droneMsgsROS/Event.h"
#include <drone_process.h>

#include "std_msgs/String.h"
#include "Definitions.h"


/*!***************************************************************************
 *  \class UserCommandLineInterpreter
 *  \brief This process collects messages from an operator in order to send 
           actions to the drone
 *****************************************************************************/
class UserCommandLineInterpreter: public DroneProcess
{
public:
  UserCommandLineInterpreter();
  ~UserCommandLineInterpreter();

public:
    void ownSetUp();
    void ownRun();
    void ownStop();
    void ownStart();

public:
  std::string drone_id;
  std::string order_topic;
  std::string current_pose_topic;
  std::string notification_topic;
  std::string order_msg = "";
  std::thread order_thread;
  std::string order;

  droneMsgsROS::dronePose current_pose;
  std_msgs::String notification;
  bool check = false; 
  bool strategy = false;

  ros::Subscriber current_pose_subscriber;
  ros::Subscriber notification_subscriber;

  ros::NodeHandle n;
  ros::Publisher order_pub;

private:
    void currentPoseCallback(const droneMsgsROS::dronePose &msg);
    void notificationCallback(const std_msgs::String &msg);
    void getOrder();

};

#endif
